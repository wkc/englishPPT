'use strict';

module.exports = function(grunt) {
    grunt.initConfig({
        watch: {
            jade: {
                files: ['index.jade', 'test.jade', 'jade/*', 'admin.jade'],
                tasks: ['jade']
            },
            html: {
                files: ['Gruntfile.js'],
                tasks: ['jsbeautifier'],
                options: {
                    // debounceDelay: 250,
                },
            },
            reload: {
                files: ['index.html', 'english.html', 'css/impress-demo.css', 'test.css'],
                // tasks: ['jsbeautifier'],
                options: {
                    livereload: true,
                },
            },
            coffee: {
                files: 'js/my.coffee',
                tasks: 'coffee'
            }
        },
        jade: {
            compile: {
                options: {
                    pretty: true
                },
                files: {
                    "index.html": "index.jade",
                    "test.html": "test.jade",
                    "admin.html": "admin.jade",
                }
            }
        },
        jsbeautifier: {
            files: ['Gruntfile.js', 'css/my.css'],
        },
        coffee: {
            compile: {
                files: {
                    'js/my.js': 'js/my.coffee'
                }
            },
        },
        clean: {
            build: {
                src: ['index.html', 'js/my.js']
            },
        }
    });

    grunt.loadNpmTasks('grunt-contrib-watch');
};
